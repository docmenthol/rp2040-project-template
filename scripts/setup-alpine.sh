apk update
apk add rustup
rustup-init -qy --default-toolchain nightly
source "$HOME/.cargo/env"
rustup target add thumbv6m-none-eabi
cargo add elf2uf2-rs